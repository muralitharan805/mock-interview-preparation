import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { LayoutComponent } from '../layout/layout.component';
import { SideNavMenuList } from '../interfaces';

@Component({
  selector: 'app-authenticated',
  standalone: true,
  imports: [
    RouterOutlet,
    CommonModule,
    RouterLink,
    RouterLinkActive,
    LayoutComponent,
  ],
  templateUrl: './authenticated.component.html',
  styleUrl: './authenticated.component.scss',
})
export class AuthenticatedComponent {
  sideNavMenuList: SideNavMenuList[];
  constructor() {
    this.sideNavMenuList = [
      {
        route_name: 'interview-practice',
        route_link: '/',
      },
      {
        route_name: 'manage-question',
        route_link: '/manage-question',
      },
    ];
  }
}
