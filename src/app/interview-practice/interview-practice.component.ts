import { CommonModule } from '@angular/common';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIcon } from '@angular/material/icon';

@Component({
  selector: 'app-interview-practice',
  standalone: true,
  imports: [CommonModule, MatButtonModule, MatIcon],
  templateUrl: './interview-practice.component.html',
  styleUrl: './interview-practice.component.scss',
})
export class InterviewPracticeComponent {
  @ViewChild('localVideo') localVideo: any;
  @ViewChild('recordedVideo') recordedVideo!: ElementRef;
  is_camera_access = false;
  is_micro_phone_access = false;
  is_start_interview = false;
  mediaStream!: MediaStream;
  questions!: Array<any>;
  current_question: any;
  temp_question!: Array<any>;
  allQuestionsShown = false;
  total_question = 0;

  constructor() {}
  ngOnInit() {}

  async accessCamera() {
    if (!this.is_camera_access) {
      const get_video_stream = await navigator.mediaDevices.getUserMedia({
        video: true,
      });

      if (this.mediaStream === undefined) {
        this.mediaStream = get_video_stream;
        this.localVideo.nativeElement.srcObject = this.mediaStream;
      } else {
        get_video_stream.getVideoTracks().forEach((data) => {
          this.mediaStream.addTrack(data);
        });
        this.localVideo.nativeElement.srcObject = new MediaStream(
          get_video_stream
        );
      }
    } else {
      this.mediaStream.getVideoTracks().forEach((data) => {
        data.stop();
      });
    }

    this.is_camera_access = !this.is_camera_access;
  }
  async accessMicroPhone() {
    const get_mic_stream = await navigator.mediaDevices.getUserMedia({
      audio: true,
    });

    if (!this.is_micro_phone_access) {
      if (this.mediaStream === undefined) {
        this.mediaStream = get_mic_stream;
      } else {
        get_mic_stream.getAudioTracks().forEach((data) => {
          this.mediaStream.addTrack(data);
        });
      }
    } else {
      this.mediaStream.getAudioTracks().forEach((data) => {
        data.stop();
      });
    }
    this.is_micro_phone_access = !this.is_micro_phone_access;
  }

  startInterview() {
    this.is_start_interview = true;
    this.initalizeQuestion();
    this.current_question = this.randomeQustion();
  }

  processInterview() {
    if (this.questions.length === 0) {
      this.allQuestionsShown = true;
      this.is_start_interview = false;
      this.mediaStream.getTracks().forEach((data) => data.stop());
      this.temp_question = this.questions;
      this.is_start_interview = false;
      this.is_camera_access = false;
      this.is_micro_phone_access = false;
    }
    this.current_question = this.randomeQustion();
  }

  randomeQustion() {
    return this.questions.splice(
      Math.floor(Math.random() * this.questions.length - 1),
      1
    )[0];
  }
  initalizeQuestion() {
    this.questions = [
      {
        question: 'whats javascript?',
      },
      {
        question: 'difference between let and var?',
      },
    ];
    this.total_question = this.questions.length;
    this.temp_question = this.questions;
  }
}
