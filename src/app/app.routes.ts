import { Routes } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoginComponent } from './login/login.component';
import { authGuard } from './auth-guard/auth.guard';
import { AuthenticatedComponent } from './authenticated/authenticated.component';
import { InterviewPracticeComponent } from './interview-practice/interview-practice.component';
import { ManageQuestionComponent } from './manage-question/manage-question.component';

export const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '',
  },
  {
    path: '',
    component: AuthenticatedComponent,
    canActivate: [authGuard],
    children: [
      {
        path: '',
        component: InterviewPracticeComponent,
      },
      { path: 'manage-question', component: ManageQuestionComponent },
    ],
  },
  { path: 'login', component: LoginComponent },
  {
    path: '**',
    component: PageNotFoundComponent,
  },
];
