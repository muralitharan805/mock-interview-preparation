import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginBody } from './interfaces';
import { of, tap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}
  userLogin(login_body: LoginBody) {
    return of({
      status: 200,
      data: {
        token: 'your_token_here', // You can customize this to simulate different responses
      },
    }).pipe(
      tap((response: any) => {
        console.log('tab ', response);
        if (response.status && response.data.token) {
          localStorage.setItem('token', response.data.token);
        }
      })
    );

    return this.http.post('http://localhost:3000/login', login_body).pipe(
      tap((response: any) => {
        console.log('tab ', response);
        if (response.status && response.data.token) {
          localStorage.setItem('token', response.data.token);
        }
      })
    );
  }
}
