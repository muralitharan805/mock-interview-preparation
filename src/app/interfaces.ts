export interface Interfaces {}
export interface LoginBody {
  user_name: string;
  password: string;
}

export interface SideNavMenuList {
  route_name: string;
  route_link: string;
}
