import { Component } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import {
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { AuthService } from '../auth.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss',
})
export class LoginComponent {
  logInForm: FormGroup;
  constructor(
    formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this.logInForm = formBuilder.group({
      user_name: ['', Validators.required],
      password: ['', Validators.required],
    });
  }
  login() {
    if (this.logInForm.valid) {
      const formValue = this.logInForm.value;
      console.log('formValue ', formValue);
      this.authService.userLogin(formValue).subscribe((data) => {
        console.log('login ', data);
        this.router.navigate(['/']);
      });
    } else {
      console.error('error ');
    }
  }
}
